# Default variables
generate=""
type=""
randomizer="default"
keyfiles_dir="$HOME/solana_keyfiles"
main_wallet="$HOME/solana/mainnet-validator-keypair.json"
# Options
. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/colors.sh) --
option_value(){ echo "$1" | sed -e 's%^--[^=]*=%%g; s%^-[^=]*=%%g'; }
while test $# -gt 0; do
	case "$1" in
	-h|--help)
		. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/logo.sh)
		echo
		echo -e "${C_LGn}Functionality${RES}: the script provides advanced CLI client features"
		echo
		echo -e "Usage: script ${C_LGn}[OPTIONS]${RES}"
		echo
		echo -e "${C_LGn}Options${RES}:"
		echo -e "  -h,  --help               show help page"
		echo -e "  -g,  --generate NUMBER    generate NUMBER wallets"
		echo -e "  -d,  --distribution       distribute certain number of tokens (see -r option) from"
		echo -e "                             the main wallet to several other wallets"
		echo -e "  -c,  --collecting         collect certain number of tokens (see -r option) from"
		echo -e "                             several wallets to the main wallet"
		echo -e "  -r,  --randomizer ARRAY   ARRAY can have a minimum and a maximum, only a maximum for"
		echo -e "                             randomizer, or no value at all to use default"
		echo -e "                             E.g. for -d option '${C_LGn}0.2 0.2${RES}' (default), '${C_LGn}0.1${RES}', '${C_LGn}0.1 0.2${RES}'"
		echo -e "                             E.g. for -c option '${C_LGn}ALL${RES}' (default), '${C_LGn}0.2 0.2${RES}', '${C_LGn}0.1${RES}', '${C_LGn}0.1 0.2${RES}'"
		echo -e "  -kd, --keyfiles-dir PATH  diroctory with wallet keyfiles (default is"
		echo -e "                             ${C_LGn}${keyfiles_dir}${RES})"
		echo -e "                             ${C_R}Don't place the main wallet keyfile there!${RES}"
		echo -e "  -mw, --main-wallet PATH   path to JSON keyfile of the main wallet"
		echo -e "                             (default is ${C_LGn}${main_wallet}${RES})"
		echo
		echo -e "You can use ${C_LGn}either${RES} \"=\" or \" \" as an option and value delimiter"
		echo
		echo -e "${C_LGn}Useful URLs${RES}:"
		echo -e "https://t.me/letskynode — node Community"
		echo
		return 0 2>/dev/null; exit 0
		;;
	-g*|--generate*)
		if ! grep -q "=" <<< "$1"; then shift; fi
		generate=`option_value "$1"`
		shift
		;;
	-d|--distribution)
		type="distribution"
		shift
		;;
	-c|--collecting)
		type="collecting"
		shift
		;;
	-r*|--randomizer*)
		if ! grep -q "=" <<< "$1"; then shift; fi
		randomizer=`option_value "$1"`
		shift
		;;
	-kd*|--keyfiles-dir*)
		if ! grep -q "=" <<< "$1"; then shift; fi
		keyfiles_dir=`option_value "$1"`
		shift
		;;
	-mw*|--main-wallet*)
		if ! grep -q "=" <<< "$1"; then shift; fi
		main_wallet=`option_value "$1"`
		shift
		;;
	*|--)
		break
		;;
	esac
done
# Functions
printf_n(){ printf "$1\n" "${@:2}"; }
current_time() { date +"%d_%m_%y_%H_%M_%S"; }
# Actions
sudo apt install wget -y &>/dev/null
. <(wget -qO- https://raw.githubusercontent.com/SecorD0/utils/main/logo.sh)
if ! solana --version &>/dev/null; then
	printf_n "${C_R}First install the Solana Tool Suite:
. <(wget -qO- https://raw.githubusercontent.com/SecorD0/Solana/main/multi_tool.sh)${RES}"
	return 1 2>/dev/null; exit 1
fi
if [ ! -n "$generate" ] && [ ! -n "$type" ]; then
	printf_n "${C_R}You didn't specified an action! Use${RES} -h ${C_R}option to view the help page${RES}"
	return 1 2>/dev/null; exit 1
fi
if [ -n "$generate" ]; then
	if printf "%d" "$generate" &>/dev/null; then
		generate=`printf "%d" $generate`
		mkdir "$keyfiles_dir"
		for i in `seq $generate`; do
			printf_n "${C_LGn}Generate wallet: ${i}${RES}"
			solana-keygen new -s --no-bip39-passphrase -o "${keyfiles_dir}/`current_time`__${i}.json"
		done
	else
		printf_n "${C_R}The specified number of wallets is not a number!${RES}"
		return 1 2>/dev/null; exit 1
	fi
fi
if [ -n "$type" ]; then
	for file_name in ${keyfiles_dir}/*.json; do
		read -r -a arr <<< "${randomizer[@]}"	
		if [ "${#arr[@]}" -eq "2" ]; then
			number=`seq "${arr[0]}" 0.01 "${arr[1]}" | shuf | head -n1`
		elif [ "${#arr[@]}" -eq "1" ] && [ "$randomizer" != "default" ] && [ "$randomizer" != "" ]; then
			number=`seq 0.01 0.01 "${arr[0]}" | shuf | head -n1`
		else
			if [ "$type" = "distribution" ]; then
				number="0.2"
			else
				number="ALL"
			fi
		fi
		if [ "$type" = "distribution" ]; then
			printf_n "Send ${C_LGn}${number} SOL${RES} to ${C_LGn}${file_name}${RES}"
			solana transfer -um --allow-unfunded-recipient --from "$main_wallet" "$file_name" "$number"
		else
			printf_n "Send ${C_LGn}${number} SOL${RES} from ${C_LGn}${file_name}${RES}"
			solana transfer -um --allow-unfunded-recipient --from "$file_name" "$main_wallet" "$number"
		fi

	done
fi
echo -e "${C_LGn}Done!${RES}"